import React, { Component } from "react";

export default class BookList extends Component {
  constructor() {
    super();
    this.state = {
      colors: "black",
      click: "click",
      unclick: "unclick",
      coId: 5,
      arr: [
        { id: 1, title: "មនុស្សពីរនាក់នៅជិតគ្នា",publishedYear:2012, isHiding: false },
        { id: 2, title: "គង់ហ៊ាន",publishedYear:2015, isHiding: false },
        { id: 3, title: "បុរសចេះថ្នាំពិសពស់",publishedYear:2018, isHiding: false },
        { id: 4, title: "អណ្ដើកនិងស្វា",publishedYear:2019, isHiding: false },
      ],
    };
  }
  changeColor = (i) => {
    let newBooks = this.state.books;
    newBooks[i.id - 1].isClicked = !newBooks[i.id - 1].isHiding;
    this.setState({
      books: newBooks,
    });
  };

  addData=()=>{
    this.setState((prevState,props)=>{
        return{coId:prevState.coId+1}
    })
    const obj = {'id':this.state.coId,'title':'haha5','isHiding':false};
    this.setState({
        books:[...this.state.books,obj]
    })
    console.log(this.state.books)

}

  render() {
    var row = this.state.books.map((items) => (
      <tr
        key={items.id}
        style={items.isHiding ? { color: "red" } : { color: "" }}
      >
        <td style={{ padding: 10 }}>{items.id}</td>
        <td style={{ padding: 10 }}>{items.title}</td>
        <td style={{ padding: 10 }}>{items.publishedYear}</td>
        <td>
          <button
            style={items.isClicked ? { color: "red" } : { color: "yellow" }}
            onClick={() => this.changeColor(items)}
          >
            {items.isClicked ? this.state.unclick : this.state.click}
          </button>
        </td>
      </tr>
    ));

    return (
      <div>
        <button onClick={this.addData}>Click Me</button>
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>Published Year</th>
              <th>Action</th>
            </tr>
          </thead>
          {row}
        </table>
      </div>
    );
  }
}
